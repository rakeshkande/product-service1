/**
 * 
 */
package com.ftd.services.product.helper;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.ftd.services.product.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftd.services.product.constants.CommonConstants;

/**
 * @author djohn
 *
 */
@Service
public class ProductServiceHelper {

	@Autowired
	JsonUtil jsonUtil;

	public String getProductData(String productId) {
		return jsonUtil.readJsonFromFile(productId + ".json");
	}

}
