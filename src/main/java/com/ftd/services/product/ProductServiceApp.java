package com.ftd.services.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main class for Product Service.
 * @author djohn
 *
 */
@SpringBootApplication
public class ProductServiceApp {

  public static void main(String[] args) {

    SpringApplication.run(ProductServiceApp.class, args);
  }

}
