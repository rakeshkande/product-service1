/**
 * 
 */
package com.ftd.services.product.service;

import java.util.List;

/**
 * @author djohn
 *
 */
public interface ProductService {

	public String getProductDetails(List<String> ids);
}
